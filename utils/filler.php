<?php
 require "config.php";
 require "lib.php";
 $colors=dosql('SELECT color_id,color_name FROM color');
 $types=dosql('SELECT type_id,type_name FROM type');
 $firms=dosql('SELECT firm.firm_id,firm_type.type_id FROM firm LEFT OUTER JOIN firm_type ON firm_type.firm_id=firm.firm_id');
 $parts=array();
 $supply=array();
 $c=1;
 $cc=1;
 for($i=0;$i<count($colors);$i++)
 {
  for($j=0;$j<count($types);$j++)
  {
   array_push($parts,'('.$c.','.$types[$j]['type_id'].",'".$colors[$i]['color_name'].' '.$types[$j]['type_name']."',".$colors[$i]['color_id'].")");
   for($k=0;$k<count($firms);$k++)
   {
    if ($firms[$k]['type_id']==$types[$j]['type_id'])
    {
     $d=rand(1,60);
     $qv=rand(1,100);
     $pr=rand(0,100);
     $ql=rand(50,100)/100;
     array_push($supply,"(".$cc.",".$firms[$k]['firm_id'].",".$c.",DATE_ADD(NOW(),INTERVAL -".$d." DAY),".$qv.",".$pr.",".$ql.",'supply".$firms[$k]['firm_id'].'_'.$types[$j]['type_id']."')");
     $cc++;
    }
   }
   $c++;
  }
 }
?>
Parts:
<BR/>
<?=join(",<BR>",$parts);
?>
<BR/>
Supply:
<BR/>
<?=join(",<BR>",$supply);
?>