<?php
 header("Content-Type: text/html; charset=windows-1251");
 require "config.php";
 require "lib.php";
 $report_id=intval($_POST['report_id']);
 $r=mysql_fetch_assoc(mysql_query('SELECT report.report_name,measure_id,measure2_id,subtotal_id,f1.field_for_name AS measurefield1,f1.field_table AS measuretable1,f1.field_field AS dbf1, f2.field_for_name AS measurefield2,f2.field_table AS measuretable2,f2.field_field AS dbf2, f2.field_id AS measure2fieldid, f3.field_for_name AS measurefield0, f3.field_table AS measuretable0,f3.field_field AS dbf0 FROM report LEFT OUTER JOIN field AS f1 ON f1.field_id=report.measure_id LEFT OUTER JOIN field AS f2 ON f2.field_id=report.measure2_id LEFT OUTER JOIN field AS f3 ON f3.field_id=report.subtotal_id WHERE report_id='.$report_id));
 $used_fields=array();
 array_push($used_fields,$r['measure_id']);
 array_push($used_fields,$r['measure2_id']);
 array_push($used_fields,$r['subtotal_id']);
 $q=mysql_query('SELECT field_id FROM report_column WHERE report_id='.$report_id.
          ' UNION SELECT field_id FROM report_filter WHERE report_id='.$report_id.
          ' UNION SELECT field_id FROM report_orderby WHERE report_id='.$report_id);
 while(list($fid)=mysql_fetch_row($q))         
 {
  array_push($used_fields,$fid);
 } 
 $jointext='';
 $t1ar=array();
 $t2ar=array();
 $q=mysql_query('SELECT field_table,join_table FROM field WHERE field_id IN ('.join(',',$used_fields).') AND join_table<>\'\'');
 while(list($t1,$t2)=mysql_fetch_row($q))
 {
  if ((!isset($t1ar[$t1])) || (!isset($t2ar[$t2])))
  {
   if (!(isset($t1ar[$t1])))
   {
    $jointext.=' LEFT OUTER JOIN '.$t1.' ON '.$t1.'.'.$t1.'_id='.$t2.'.'.$t1.'_id ';
   } 
   if ((!isset($t1ar[$t2])) && ($t2!='supply'))
   {
    $jointext=' LEFT OUTER JOIN '.$t2.' ON '.$t2.'.'.$t2.'_id=supply.'.$t2.'_id '.$jointext;
    $t1ar[$t2]=1;
   }
   $t1ar[$t1]=1;
   $t2ar[$t2]=1;
  }
 }
 if ($r['measure_id'])
 {
  if (strpos($r['dbf1'],' AS '))
  {
   $sql='SELECT '.$r['dbf1'];
  }
  else
  {
   $sql='SELECT '.$r['measuretable1'].'.'.$r['measurefield1'];
  } 
  if ($r['measure2_id'])
  {
   $sql.=','.$r['measuretable2'].'.'.$r['measurefield2'];  
  }
 } 
 else
 {
  $sql='SELECT '.$r['measuretable2'].'.'.$r['measurefield2'];
 }
 if ($r['subtotal_id'])
 {
  $sql.=','.$r['measuretable0'].'.'.$r['measurefield0'];
 }
 $cols=array();
 $colids=array();
 $colnames=array();
 $fn=array(); 
 $q=mysql_query('SELECT report_column.field_id,field_for_name,field_table,field_field,fn_sql FROM report_column LEFT OUTER JOIN field ON field.field_id=report_column.field_id LEFT OUTER JOIN fn ON report_column.fn_id=fn.fn_id WHERE report_id='.$report_id.' ORDER BY field.field_id');
 while($r2=mysql_fetch_assoc($q))
 {
  array_push($colids,$r2['field_id']);
  array_push($colnames,$r2['field_for_name']);
  if ($r2['fn_sql'])
  {
   if (preg_match('/^(.*?) AS /',$r2['field_field'],$matches))
   {
       array_push($cols,$r2['fn_sql'].'('.$matches[1].') AS '.$r2['field_for_name']);
   }
   else
   {
    array_push($cols,$r2['fn_sql'].'('.$r2['field_table'].'.'.$r2['field_for_name'].') AS '.$r2['field_for_name']);    
   }
   array_push($fn,$r2['fn_sql']);
  }
  else
  {
   array_push($cols,$r2['field_table'].'.'.$r2['field_for_name']);
  }
 }
 $sql.=','.join(',',$cols).' FROM supply '.$jointext;
 $whereclause=array();
 $filters=array();
 $q=mysql_query('SELECT field_name,field_field,field_table,field_for_name,default_value,signum.signum_name,dateinterval_sql,dateinterval_name FROM report_filter LEFT OUTER JOIN field ON field.field_id=report_filter.field_id LEFT OUTER JOIN signum ON signum.signum_id=report_filter.signum_id LEFT OUTER JOIN dateinterval ON dateinterval.dateinterval_id=report_filter.default_value WHERE report_id='.$report_id);
 while($rf=mysql_fetch_assoc($q))
 {
  if ($rf['signum_name']=='')
  {
   $rf['signum_name']='=';
  }
  if ($rf['field_field']=='supply_date')
  {
   if (isset($_POST['date_startDay']))
   {
    array_push($whereclause,'supply_date>='.ToMysql('date_start').' AND supply_date<='.ToMysql('date_end'));
    array_push($filters,array('name'=>'��������� ��� = ','value'=>humandate('date_start').'-'.humandate('date_end')));
   }
   else 
   {
    array_push($whereclause,$rf['dateinterval_sql']);
    array_push($filters,array('name'=>'��������� ��� = ','value'=>$rf['dateinterval_name']));
   }
  }
  else 
  {
  if (!(isset($_POST[$rf['field_field']])))
  {
   if ($rf['default_value']!=0)
   {
    array_push($whereclause,$rf['field_table'].'.'.$rf['field_field'].$rf['signum_name'].quot($rf['default_value']).' ');
    if ($rf['field_table']!='supply')
    {
     list($name)=mysql_fetch_row(mysql_query('SELECT '.$rf['field_table'].'_name FROM '.$rf['field_table'].' WHERE '.$rf['field_table'].'_id='.$rf['default_value']));
    }
    else
    {
     $name=$rf['default_value'];
    }
   }
  }
  else
  {
   array_push($whereclause,$rf['field_table'].'.'.$rf['field_field'].$rf['signum_name'].quot($_POST[$rf['field_field']]).' ');     
    if ($rf['field_table']!='supply')
    {
     list($name)=mysql_fetch_row(mysql_query('SELECT '.$rf['field_table'].'_name FROM '.$rf['field_table'].' WHERE '.$rf['field_table'].'_id='.intval($_POST[$rf['field_field']])));
    }
    else
    {
     $name=$_POST[$rf['field_field']];
    }
  }
  array_push($filters,array('name'=>$rf['field_name'],'value'=>$rf['signum_name'].' '.$name));
  }
 }
 if (count($whereclause)>0)
 {
  $sql.=' WHERE '.join(' AND ',$whereclause);
 } 
 $groupby=array();
 for($i=0;$i<3;$i++)
 {
  if ($r['dbf'.$i])
  {
   if (strpos($r['dbf'.$i],' AS '))
   {
    array_push($groupby,$r['measurefield'.$i]);
   }
   else
   {
    array_push($groupby,$r['measuretable'.$i].'.'.$r['dbf'.$i]);
   } 
  }
 }
 if (count($groupby)>0)
 {
  $sql.=' GROUP BY '.join(',',$groupby);
 }
 $orderby=array();
 if ($r['measurefield0'])
 {
  array_push($orderby,$r['measuretable0'].'.'.$r['measurefield0']);
 }
 $q=mysql_query('SELECT field_field,field_table,field_for_name,dir_sql FROM report_orderby LEFT OUTER JOIN field ON field.field_id=report_orderby.field_id LEFT OUTER JOIN dir ON dir.dir_id=report_orderby.dir_id WHERE report_id='.$report_id);
 while($orderbyr=mysql_fetch_assoc($q))
 {
  if (strpos($orderbyr['field_field'],' AS '))
  {
   array_push($orderby,$orderbyr['field_for_name'].' '.$orderbyr['dir_sql']);	
  }
  else {
   array_push($orderby,$orderbyr['field_table'].'.'.$orderbyr['field_for_name'].' '.$orderbyr['dir_sql']);	
  }  
 }
 if (count($orderby)>0)
 {
  $sql.=' ORDER BY '.join(',',$orderby);
 }
 $data=array();
 array_push($data,''); 
 $start=1;
 $colstotal=1;
# if ($r['subtotal_id'])
#{
#  array_push($data,'');
#  $start=2;
# }
 if ($r['measure2_id'])
 {
  list($orderbyf)=mysql_fetch_row(mysql_query('SELECT dir_sql FROM report_orderby LEFT OUTER JOIN dir ON dir.dir_id=report_orderby.dir_id WHERE report_id='.$report_id.' AND field_id='.$r['measure2fieldid']));
  $q2=mysql_query('SELECT '.$r['measurefield2'].' AS name FROM '.$r['measuretable2'].' ORDER BY name '.$orderbyf);
  while(list($n)=mysql_fetch_row($q2))
  {
   array_push($data,$n);
   $colstotal++;
  }
 }
 else 
 {
  $q=mysql_query('SELECT field_name,fn_name FROM report_column LEFT OUTER JOIN field ON field.field_id=report_column.field_id LEFT OUTER JOIN fn ON fn.fn_id=report_column.fn_id WHERE report_id='.$report_id.' ORDER BY field.field_id');
  while(list($fid,$fname)=mysql_fetch_row($q))
  {
   array_push($data,$fid.'('.$fname.')');
  }
  $colstotal+=count($cols);
 }
 $count=0;
 $measure1='';
 $subtotal='';
 $counter2=0;
 $d=array();
 $dtotal=array();
 for($j=0;$j<count($cols);$j++)
 {
  if ($fn[$j]=='MIN')
  {
   $dtotal[$j]=PLUSINF;
   $d[$j]=PLUSINF;
  }
  else
  {
   $dtotal[$j]=0;
   $d[$j]=0;
  }
 }
 $counter3=0;
 #print $sql;
 $q=mysql_query($sql);
 while($res=mysql_fetch_assoc($q))
 {
   if ($r['measurefield0'])   
   {
    if ($res[$r['measurefield0']]!=$subtotal)   
    {
     if ($subtotal)
     {
     $counter2=0;
     array_push($data,'��������:'.$subtotal);
     for($j=0;$j<count($cols);$j++)
     {
      array_push($data,$d[$j]);
      $count++;
      switch ($fn[$j])
      {
       case 'SUM':
        $dtotal[$j]+=$d[$j];
        $d[$j]=0;
        break;
       case 'AVG':
        $dtotal[$j]=($dtotal[$j]*$counter3+$d[$j])/($counter3+1);
        $counter3++;
        $d[$j]=0;
        break;
       case 'MAX':
        if ($dtotal[$j]<$d[$j])
        {
         $dtotal[$j]=$d[$j];
        }
        $d[$j]=0;
        break;
       case 'MIN':
        if ($dtotal[$j]>$d[$j])
        {
         $dtotal[$j]=$d[$j];
        }
        $d[$j]=PLUSINF;         
        break;
      }
     }
     }
     $subtotal=$res[$r['measurefield0']];
    }
    for($j=0;$j<count($cols);$j++)
    {
     switch  ($fn[$j])
     {
      case 'SUM':
       $d[$j]+=$res[$colnames[$j]];
       break;
      case 'MIN':
       if ($d[$j]>$res[$colnames[$j]])
       {
        $d[$j]=$res[$colnames[$j]];
       }
       break;
       case 'MAX':
       if ($d[$j]<$res[$colnames[$j]])
       {
        $d[$j]=$res[$colnames[$j]];
       }
       break;
       case 'AVG':
        $d[$j]=($d[$j]*$counter2+$res[$colnames[$j]])/($counter2+1);
        $counter2++;
       break;
     }
    }    
   }
  if($measure1!=$res[$r['measurefield1']])
  {
   $measure1=$res[$r['measurefield1']];
   array_push($data,$measure1);
  }
  if ($r['measurefield2'])
  {
   for($j=$start;$j<$colstotal;$j++)
   {
    if ($res[$r['measurefield2']]===$data[$j])
    {
     array_push($data,$res[$colnames[0]]);       
     if ($j<$colstotal-1)
     {
      $res=mysql_fetch_assoc($q);
     } 
    }
    else
    {
     array_push($data,'&nbsp;');
    }
   }
  } 
   else
   {
    for($j=0;$j<count($cols);$j++)
    {
     array_push($data,$res[$colnames[$j]]);
    }
   }   
   $count++;
 }
 #������� ��������� ��������� � ������
 if ($r['measurefield0'])
 {
  array_push($data,'��������:'.$subtotal);
     for($j=0;$j<count($cols);$j++)
     {
      array_push($data,$d[$j]);
      $count++;
     }
     array_push($data,'�����:'); 
     for($j=0;$j<count($cols);$j++)
     {
      array_push($data,$dtotal[$j]);
      $count++;
     }

 }
 $smarty->assign('cols',$colstotal);
 $smarty->assign('data',$data);
 $smarty->assign('filters',$filters);
 $smarty->assign('title',$r['report_name']);
 $smarty->assign('body','reportbody.tpl');
 $smarty->display('index.tpl');
 ?>