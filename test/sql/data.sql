DELETE fROM city;
INSERT INTO city(city_id,city_name)
VALUES
(1,'������'),
(2,'�����'),
(3,'�������'),
(4,'������'),
(5,'�������');
DELETE fROM type;
INSERT INTO type(type_id,type_name)
VALUES
(1,'��������'),
(2,'�����'),
(3,'�����'),
(4,'���������'),
(5,'������');
/*
DELETE fROM model;
INSERT INTO model(model_id,model_name)
VALUES
(1,'VAZ 110'),
(2,'VAZ 111'),
(3,'VAZ 112');
*/
DELETE fROM color;
INSERT INTO color(color_id,color_name)
VALUES
(1,'�������'),
(2,'�������'),
(3,'�����'),
(4,'������');
DELETE fROM status;
INSERT INTO status(status_id,status_name)
VALUES
(1,'�������'),
(2,'�������'),
(3,'����������� �������');
DELETE fROM firm;
INSERT INTO firm(firm_id,city_id,firm_name,status_id,discount)
VALUES
(1,1,'��� ���������� ������',3,0.89),
(2,1,'��� �&�',1,0.8),
(3,2,'�������',2,0.9),
(4,2,'��������� ������',3,1.0),
(5,3,'�� ����������� ������',1,0.99),
(6,4,'��������� ������',2,0.9),
(7,5,'����������� ������',3,0.9);
DELETE FROM firm_type;
INSERT INTO firm_type(firm_id,type_id)
VALUES
(1,1),(1,2),(1,3),
(2,1),(2,2),(2,3),(2,4),(2,5),
(3,1),(3,2),(3,3),(3,4),(3,5),
(4,5),
(5,1),(5,2),(5,3),
(6,4),
(7,1),(7,2),(7,3)
;
DELETE FROM part;
INSERT INTO part(part_id,type_id,part_name,color_id)
VALUES
(1,1,'������� ��������',1),
(2,2,'������� �����',1),
(3,3,'������� �����',1),
(4,4,'��������� V8',0),
(5,5,'������ 13`',0),
(6,1,'������� ��������',2),
(7,2,'������� �����',2),
(8,3,'������� �����',2),
(9,4,'��������� V4',0),
(10,5,'������ 15`',0),
(11,1,'����� ��������',3),
(12,2,'����� �����',3),
(13,3,'����� �����',3),
(14,4,'��������� R4',0),
(15,5,'������ 17`',0),
(16,1,'������ ��������',4),
(17,2,'������ �����',4),
(18,3,'������ �����',4),
(19,4,'��������� R8',0),
(20,5,'������ 19`',0)
;
DELETE FROM supply;
INSERT INTO supply(supply_id,firm_id,part_id,supply_date,quantity,price,quality,supply_code)
VALUES
(1,1,1,DATE_ADD(NOW(),INTERVAL -36 DAY),79,27,0.58,'supply1_1'),
(2,2,1,DATE_ADD(NOW(),INTERVAL -60 DAY),16,68,0.6,'supply2_1'),
(3,3,1,DATE_ADD(NOW(),INTERVAL -2 DAY),15,14,0.93,'supply3_1'),
(4,5,1,DATE_ADD(NOW(),INTERVAL -25 DAY),10,14,0.59,'supply5_1'),
(5,7,1,DATE_ADD(NOW(),INTERVAL -23 DAY),85,64,0.91,'supply7_1'),
(6,1,2,DATE_ADD(NOW(),INTERVAL -39 DAY),74,10,0.91,'supply1_2'),
(7,2,2,DATE_ADD(NOW(),INTERVAL -26 DAY),63,50,0.9,'supply2_2'),
(8,3,2,DATE_ADD(NOW(),INTERVAL -27 DAY),85,30,0.93,'supply3_2'),
(9,5,2,DATE_ADD(NOW(),INTERVAL -54 DAY),25,48,0.78,'supply5_2'),
(10,7,2,DATE_ADD(NOW(),INTERVAL -32 DAY),17,50,0.51,'supply7_2'),
(11,1,3,DATE_ADD(NOW(),INTERVAL -34 DAY),45,32,0.92,'supply1_3'),
(12,2,3,DATE_ADD(NOW(),INTERVAL -41 DAY),45,43,0.54,'supply2_3'),
(13,3,3,DATE_ADD(NOW(),INTERVAL -8 DAY),99,79,0.69,'supply3_3'),
(14,5,3,DATE_ADD(NOW(),INTERVAL -38 DAY),43,86,0.91,'supply5_3'),
(15,7,3,DATE_ADD(NOW(),INTERVAL -24 DAY),62,62,0.99,'supply7_3'),
(16,2,4,DATE_ADD(NOW(),INTERVAL -9 DAY),88,53,0.97,'supply2_4'),
(17,3,4,DATE_ADD(NOW(),INTERVAL -6 DAY),8,57,0.66,'supply3_4'),
(18,6,4,DATE_ADD(NOW(),INTERVAL -58 DAY),55,19,0.6,'supply6_4'),
(19,2,5,DATE_ADD(NOW(),INTERVAL -58 DAY),13,38,0.59,'supply2_5'),
(20,3,5,DATE_ADD(NOW(),INTERVAL -50 DAY),17,60,0.68,'supply3_5'),
(21,4,5,DATE_ADD(NOW(),INTERVAL -46 DAY),51,81,0.67,'supply4_5'),
(22,1,6,DATE_ADD(NOW(),INTERVAL -30 DAY),50,49,0.59,'supply1_1'),
(23,2,6,DATE_ADD(NOW(),INTERVAL -15 DAY),98,61,0.75,'supply2_1'),
(24,3,6,DATE_ADD(NOW(),INTERVAL -43 DAY),29,63,0.7,'supply3_1'),
(25,5,6,DATE_ADD(NOW(),INTERVAL -9 DAY),29,53,0.73,'supply5_1'),
(26,7,6,DATE_ADD(NOW(),INTERVAL -15 DAY),30,76,0.88,'supply7_1'),
(27,1,7,DATE_ADD(NOW(),INTERVAL -15 DAY),18,31,0.96,'supply1_2'),
(28,2,7,DATE_ADD(NOW(),INTERVAL -50 DAY),26,64,0.51,'supply2_2'),
(29,3,7,DATE_ADD(NOW(),INTERVAL -16 DAY),40,71,0.83,'supply3_2'),
(30,5,7,DATE_ADD(NOW(),INTERVAL -14 DAY),94,100,0.97,'supply5_2'),
(31,7,7,DATE_ADD(NOW(),INTERVAL -58 DAY),72,48,0.71,'supply7_2'),
(32,1,8,DATE_ADD(NOW(),INTERVAL -11 DAY),8,61,0.62,'supply1_3'),
(33,2,8,DATE_ADD(NOW(),INTERVAL -5 DAY),87,36,0.99,'supply2_3'),
(34,3,8,DATE_ADD(NOW(),INTERVAL -26 DAY),43,20,0.85,'supply3_3'),
(35,5,8,DATE_ADD(NOW(),INTERVAL -24 DAY),60,11,0.51,'supply5_3'),
(36,7,8,DATE_ADD(NOW(),INTERVAL -44 DAY),74,55,0.52,'supply7_3'),
(37,2,9,DATE_ADD(NOW(),INTERVAL -38 DAY),67,48,0.68,'supply2_4'),
(38,3,9,DATE_ADD(NOW(),INTERVAL -50 DAY),76,38,0.52,'supply3_4'),
(39,6,9,DATE_ADD(NOW(),INTERVAL -33 DAY),83,21,0.86,'supply6_4'),
(40,2,10,DATE_ADD(NOW(),INTERVAL -30 DAY),24,46,0.73,'supply2_5'),
(41,3,10,DATE_ADD(NOW(),INTERVAL -54 DAY),82,7,0.93,'supply3_5'),
(42,4,10,DATE_ADD(NOW(),INTERVAL -28 DAY),93,53,0.5,'supply4_5'),
(43,1,11,DATE_ADD(NOW(),INTERVAL -26 DAY),40,79,0.75,'supply1_1'),
(44,2,11,DATE_ADD(NOW(),INTERVAL -30 DAY),58,33,0.73,'supply2_1'),
(45,3,11,DATE_ADD(NOW(),INTERVAL -53 DAY),32,12,0.72,'supply3_1'),
(46,5,11,DATE_ADD(NOW(),INTERVAL -19 DAY),95,64,0.77,'supply5_1'),
(47,7,11,DATE_ADD(NOW(),INTERVAL -1 DAY),32,84,0.69,'supply7_1'),
(48,1,12,DATE_ADD(NOW(),INTERVAL -42 DAY),77,18,0.52,'supply1_2'),
(49,2,12,DATE_ADD(NOW(),INTERVAL -35 DAY),15,66,0.55,'supply2_2'),
(50,3,12,DATE_ADD(NOW(),INTERVAL -24 DAY),81,72,0.84,'supply3_2'),
(51,5,12,DATE_ADD(NOW(),INTERVAL -20 DAY),58,34,0.68,'supply5_2'),
(52,7,12,DATE_ADD(NOW(),INTERVAL -8 DAY),80,100,0.61,'supply7_2'),
(53,1,13,DATE_ADD(NOW(),INTERVAL -1 DAY),33,65,0.94,'supply1_3'),
(54,2,13,DATE_ADD(NOW(),INTERVAL -41 DAY),51,77,0.71,'supply2_3'),
(55,3,13,DATE_ADD(NOW(),INTERVAL -22 DAY),18,32,0.71,'supply3_3'),
(56,5,13,DATE_ADD(NOW(),INTERVAL -47 DAY),68,79,0.5,'supply5_3'),
(57,7,13,DATE_ADD(NOW(),INTERVAL -9 DAY),86,11,0.88,'supply7_3'),
(58,2,14,DATE_ADD(NOW(),INTERVAL -11 DAY),6,79,0.87,'supply2_4'),
(59,3,14,DATE_ADD(NOW(),INTERVAL -7 DAY),13,78,0.8,'supply3_4'),
(60,6,14,DATE_ADD(NOW(),INTERVAL -39 DAY),40,54,0.69,'supply6_4'),
(61,2,15,DATE_ADD(NOW(),INTERVAL -1 DAY),73,4,0.85,'supply2_5'),
(62,3,15,DATE_ADD(NOW(),INTERVAL -55 DAY),45,78,0.83,'supply3_5'),
(63,4,15,DATE_ADD(NOW(),INTERVAL -35 DAY),42,69,0.93,'supply4_5'),
(64,1,16,DATE_ADD(NOW(),INTERVAL -44 DAY),96,26,0.67,'supply1_1'),
(65,2,16,DATE_ADD(NOW(),INTERVAL -35 DAY),94,45,0.88,'supply2_1'),
(66,3,16,DATE_ADD(NOW(),INTERVAL -52 DAY),69,74,0.58,'supply3_1'),
(67,5,16,DATE_ADD(NOW(),INTERVAL -46 DAY),5,7,0.59,'supply5_1'),
(68,7,16,DATE_ADD(NOW(),INTERVAL -2 DAY),37,95,0.96,'supply7_1'),
(69,1,17,DATE_ADD(NOW(),INTERVAL -53 DAY),50,32,0.96,'supply1_2'),
(70,2,17,DATE_ADD(NOW(),INTERVAL -1 DAY),77,66,0.64,'supply2_2'),
(71,3,17,DATE_ADD(NOW(),INTERVAL -41 DAY),3,94,0.82,'supply3_2'),
(72,5,17,DATE_ADD(NOW(),INTERVAL -34 DAY),62,61,0.53,'supply5_2'),
(73,7,17,DATE_ADD(NOW(),INTERVAL -54 DAY),40,66,0.57,'supply7_2'),
(74,1,18,DATE_ADD(NOW(),INTERVAL -24 DAY),69,55,1,'supply1_3'),
(75,2,18,DATE_ADD(NOW(),INTERVAL -18 DAY),35,25,0.58,'supply2_3'),
(76,3,18,DATE_ADD(NOW(),INTERVAL -18 DAY),72,23,0.91,'supply3_3'),
(77,5,18,DATE_ADD(NOW(),INTERVAL -38 DAY),64,47,0.74,'supply5_3'),
(78,7,18,DATE_ADD(NOW(),INTERVAL -60 DAY),46,41,0.64,'supply7_3'),
(79,2,19,DATE_ADD(NOW(),INTERVAL -39 DAY),2,4,0.9,'supply2_4'),
(80,3,19,DATE_ADD(NOW(),INTERVAL -16 DAY),66,84,0.57,'supply3_4'),
(81,6,19,DATE_ADD(NOW(),INTERVAL -5 DAY),23,75,0.96,'supply6_4'),
(82,2,20,DATE_ADD(NOW(),INTERVAL -50 DAY),7,24,0.58,'supply2_5'),
(83,3,20,DATE_ADD(NOW(),INTERVAL -42 DAY),3,31,0.76,'supply3_5'),
(84,4,20,DATE_ADD(NOW(),INTERVAL -27 DAY),44,40,0.54,'supply4_5')
;
