DELETE fROM field;
INSERT INTO field
(field_id,field_name,field_table,field_field,field_for_name,
canbe_measure,
canbe_column,
canbe_filter,
canbe_subtotal,
subtotal_parent_id,
canbe_orderby,
filter_group,
is_grouping,
join_table
)
VALUES
(1,'�����','city','city_id','city_name',1,0,1,1,2,1,1,1,'firm'),
(2,'���������','firm','firm_id','firm_name',1,0,1,0,0,1,1,1,'supply'),
(3,'������ ����������','status','status_id','status_name',1,0,1,1,2,1,1,1,'firm'),
(4,'��� ������','type','type_id','type_name',1,0,1,1,5,1,2,1,'part'),
(5,'������','part','part_id','part_name',1,1,1,0,0,1,2,1,'supply'),
(6,'���� ������','color','color_id','color_name',1,1,1,1,5,1,2,1,'part'),
(7,'��������','supply','supply_id','supply_code',1,0,0,0,0,0,0,0,''),
(8,'���� �� �������','supply','DATE_FORMAT(supply_date,''%m'') AS datem','datem',1,0,0,1,10,1,0,1,''),
(9,'���� �� �������','supply','DATE_FORMAT(supply_date,''%u'') AS datew','datew',1,0,0,1,10,1,0,1,''),
(10,'���� �� ����','supply','supply_date','supply_date',1,1,1,1,0,1,0,1,''),
(11,'����','supply','price','price',0,1,1,0,0,1,0,1,''),
(12,'���������','supply','price*quantity AS cost','cost',0,1,1,0,0,1,0,1,''),
(13,'����������','supply','quantity','quantity',0,1,1,0,0,1,0,1,''),
(14,'��������','supply','quality','quality',0,1,1,0,0,1,0,1,'');
DELETE fROM field_measure_restriction;
INSERT INTO field_measure_restriction
(field_id,measure_id)
VALUES
(6,7),
(10,7),
(5,7);
/*
(13,5),
(13,4),
(14,5),
(14,4);
*/
DELETE FROM fn;
INSERT INTO fn
(fn_id,fn_name,fn_sql)
VALUES
(1,'�����','SUM'),
(2,'�������','AVG'),
(3,'��������','MAX'),
(4,'�������','MIN');
DELETE FROM dateinterval;
INSERT INTO dateinterval
(dateinterval_id,dateinterval_name,dateinterval_sql,date_start,date_end)
/*
dateinterval_sql - SQL ��� Whereclause ��� ������, ���� ������������ �� ����� ������ ���������
date_start,date_end - ��������� ��� DATEADD ��� ���������� ���� ��� ��������� ��������� �������� �������
*/
VALUES
(1,'��������� 3 ���','supply_date>=DATE_ADD(NOW(),INTERVAL -3 DAY)','-3','0'),
(2,'��������� ������','supply_date>=DATE_ADD(NOW(),INTERVAL -7 DAY)','-7','0'),
(3,'��������� �����','supply_date>=DATE_ADD(NOW(),INTERVAL -30 DAY)','-30','0');
DELETE fROM signum;
INSERT INTO signum
(signum_id,signum_name)
VALUES
(1,'>'),
(2,'<');
DELETE FROM dir;
INSERT INTO dir
(dir_id,dir_name,dir_sql)
VALUES
(1,'�� �����������','ASC'),
(2,'�� ��������','DESC');
