-- MySQL dump 10.9
--
-- Host: localhost    Database: reports
-- ------------------------------------------------------
-- Server version	4.1.16-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `city_id` int(11) NOT NULL auto_increment,
  `city_name` varchar(100) character set utf8 default NULL,
  PRIMARY KEY  (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `city`
--


/*!40000 ALTER TABLE `city` DISABLE KEYS */;
LOCK TABLES `city` WRITE;
INSERT INTO `city` VALUES (1,'Ìîñêâà'),(2,'Ïèòåð'),(3,'Ñàðàòîâ'),(4,'Ñàìàðà'),(5,'Óðåíãîé');
UNLOCK TABLES;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
CREATE TABLE `color` (
  `color_id` int(11) NOT NULL auto_increment,
  `color_name` varchar(100) character set utf8 default NULL,
  PRIMARY KEY  (`color_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `color`
--


/*!40000 ALTER TABLE `color` DISABLE KEYS */;
LOCK TABLES `color` WRITE;
INSERT INTO `color` VALUES (1,'êðàñíûé'),(2,'çåëåíûé'),(3,'ñèíèé'),(4,'æåëòûé');
UNLOCK TABLES;
/*!40000 ALTER TABLE `color` ENABLE KEYS */;

--
-- Table structure for table `dateinterval`
--

DROP TABLE IF EXISTS `dateinterval`;
CREATE TABLE `dateinterval` (
  `dateinterval_id` int(11) NOT NULL auto_increment,
  `dateinterval_name` varchar(100) character set utf8 default NULL,
  `dateinterval_sql` varchar(100) default NULL,
  `date_start` varchar(100) default NULL,
  `date_end` varchar(100) default NULL,
  PRIMARY KEY  (`dateinterval_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `dateinterval`
--


/*!40000 ALTER TABLE `dateinterval` DISABLE KEYS */;
LOCK TABLES `dateinterval` WRITE;
INSERT INTO `dateinterval` VALUES (1,'ïîñëåäíèå 3 äíÿ','supply_date>=DATE_ADD(NOW(),INTERVAL -3 DAY)','-3','0'),(2,'ïîñëåäíÿÿ íåäåëÿ','supply_date>=DATE_ADD(NOW(),INTERVAL -7 DAY)','-7','0'),(3,'ïîñëåäíèé ìåñÿö','supply_date>=DATE_ADD(NOW(),INTERVAL -30 DAY)','-30','0');
UNLOCK TABLES;
/*!40000 ALTER TABLE `dateinterval` ENABLE KEYS */;

--
-- Table structure for table `dir`
--

DROP TABLE IF EXISTS `dir`;
CREATE TABLE `dir` (
  `dir_id` int(11) NOT NULL auto_increment,
  `dir_name` varchar(100) character set utf8 default NULL,
  `dir_sql` varchar(100) default NULL,
  PRIMARY KEY  (`dir_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `dir`
--


/*!40000 ALTER TABLE `dir` DISABLE KEYS */;
LOCK TABLES `dir` WRITE;
INSERT INTO `dir` VALUES (1,'Ïî âîçðàñòàíèþ','ASC'),(2,'Ïî óáûâàíèþ','DESC');
UNLOCK TABLES;
/*!40000 ALTER TABLE `dir` ENABLE KEYS */;

--
-- Table structure for table `field`
--

DROP TABLE IF EXISTS `field`;
CREATE TABLE `field` (
  `field_id` int(11) NOT NULL auto_increment,
  `field_name` varchar(100) character set utf8 default NULL,
  `field_table` varchar(200) default NULL,
  `field_field` varchar(200) default NULL,
  `field_for_name` varchar(200) default NULL,
  `canbe_measure` int(11) default '1',
  `canbe_column` int(11) default '1',
  `canbe_filter` int(11) default '1',
  `canbe_subtotal` int(11) default '1',
  `subtotal_parent_id` int(11) default '0',
  `canbe_orderby` int(11) default '1',
  `filter_group` int(11) default NULL,
  `is_grouping` int(11) default '1',
  `join_table` varchar(100) default '',
  PRIMARY KEY  (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `field`
--


/*!40000 ALTER TABLE `field` DISABLE KEYS */;
LOCK TABLES `field` WRITE;
INSERT INTO `field` VALUES (1,'Ãîðîä','city','city_id','city_name',1,0,1,1,2,1,1,1,'firm'),(2,'Ïîñòàâùèê','firm','firm_id','firm_name',1,0,1,0,0,1,1,1,'supply'),(3,'Ñòàòóñ ïîñòàâùèêà','status','status_id','status_name',1,0,1,1,2,1,1,1,'firm'),(4,'Òèï äåòàëè','type','type_id','type_name',1,0,1,1,5,1,2,1,'part'),(5,'Äåòàëü','part','part_id','part_name',1,1,1,0,0,1,2,1,'supply'),(6,'Öâåò äåòàëè','color','color_id','color_name',1,1,1,1,5,1,2,1,'part'),(7,'Ïîñòàâêà','supply','supply_id','supply_code',1,0,0,0,0,0,0,0,''),(8,'Äàòà ïî ìåñÿöàì','supply','DATE_FORMAT(supply_date,\'%m\') AS datem','datem',1,0,0,1,10,1,0,1,''),(9,'Äàòà ïî íåäåëÿì','supply','DATE_FORMAT(supply_date,\'%u\') AS datew','datew',1,0,0,1,10,1,0,1,''),(10,'Äàòà ïî äíÿì','supply','supply_date','supply_date',1,1,1,1,0,1,0,1,''),(11,'Öåíà','supply','price','price',0,1,1,0,0,1,0,1,''),(12,'Ñòîèìîñòü','supply','price*quantity AS cost','cost',0,1,1,0,0,1,0,1,''),(13,'Êîëè÷åñòâî','supply','quantity','quantity',0,1,1,0,0,1,0,1,''),(14,'Êà÷åñòâî','supply','quality','quality',0,1,1,0,0,1,0,1,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `field` ENABLE KEYS */;

--
-- Table structure for table `field_measure_restriction`
--

DROP TABLE IF EXISTS `field_measure_restriction`;
CREATE TABLE `field_measure_restriction` (
  `field_id` int(11) NOT NULL default '0',
  `measure_id` int(11) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `field_measure_restriction`
--


/*!40000 ALTER TABLE `field_measure_restriction` DISABLE KEYS */;
LOCK TABLES `field_measure_restriction` WRITE;
INSERT INTO `field_measure_restriction` VALUES (6,7),(10,7),(5,7);
UNLOCK TABLES;
/*!40000 ALTER TABLE `field_measure_restriction` ENABLE KEYS */;

--
-- Table structure for table `firm`
--

DROP TABLE IF EXISTS `firm`;
CREATE TABLE `firm` (
  `firm_id` int(11) NOT NULL auto_increment,
  `city_id` int(11) NOT NULL default '0',
  `firm_name` varchar(100) character set utf8 default NULL,
  `status_id` int(11) NOT NULL default '0',
  `discount` float default NULL,
  PRIMARY KEY  (`firm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `firm`
--


/*!40000 ALTER TABLE `firm` DISABLE KEYS */;
LOCK TABLES `firm` WRITE;
INSERT INTO `firm` VALUES (1,1,'ÎÎÎ Ìîñêîâñêèå äåòàëè',3,0.89),(2,1,'ÇÀÎ Ì&Í',1,0.8),(3,2,'Ïèòåðöû',2,0.9),(4,2,'Ïèòåðñêèå êîëåñà',3,1),(5,3,'ÀÎ Ñàðàòîâñêèå äåòàëè',1,0.99),(6,4,'Ñàìàðñêèå ìîòîðû',2,0.9),(7,5,'Óðåíãîéñêèå êóçîâà',3,0.9);
UNLOCK TABLES;
/*!40000 ALTER TABLE `firm` ENABLE KEYS */;

--
-- Table structure for table `firm_type`
--

DROP TABLE IF EXISTS `firm_type`;
CREATE TABLE `firm_type` (
  `firm_id` int(11) NOT NULL default '0',
  `type_id` int(11) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `firm_type`
--


/*!40000 ALTER TABLE `firm_type` DISABLE KEYS */;
LOCK TABLES `firm_type` WRITE;
INSERT INTO `firm_type` VALUES (1,1),(1,2),(1,3),(2,1),(2,2),(2,3),(2,4),(2,5),(3,1),(3,2),(3,3),(3,4),(3,5),(4,5),(5,1),(5,2),(5,3),(6,4),(7,1),(7,2),(7,3);
UNLOCK TABLES;
/*!40000 ALTER TABLE `firm_type` ENABLE KEYS */;

--
-- Table structure for table `fn`
--

DROP TABLE IF EXISTS `fn`;
CREATE TABLE `fn` (
  `fn_id` int(11) NOT NULL auto_increment,
  `fn_name` varchar(100) character set utf8 default NULL,
  `fn_sql` varchar(100) default NULL,
  PRIMARY KEY  (`fn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `fn`
--


/*!40000 ALTER TABLE `fn` DISABLE KEYS */;
LOCK TABLES `fn` WRITE;
INSERT INTO `fn` VALUES (1,'Ñóììà','SUM'),(2,'Ñðåäíåå','AVG'),(3,'Ìàêñèìóì','MAX'),(4,'Ìèíèìóì','MIN');
UNLOCK TABLES;
/*!40000 ALTER TABLE `fn` ENABLE KEYS */;

--
-- Table structure for table `part`
--

DROP TABLE IF EXISTS `part`;
CREATE TABLE `part` (
  `part_id` int(11) NOT NULL auto_increment,
  `type_id` int(11) NOT NULL default '0',
  `part_name` varchar(100) character set utf8 default NULL,
  `color_id` int(11) default NULL,
  PRIMARY KEY  (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `part`
--


/*!40000 ALTER TABLE `part` DISABLE KEYS */;
LOCK TABLES `part` WRITE;
INSERT INTO `part` VALUES (1,1,'êðàñíûé áàãàæíèê',1),(2,2,'êðàñíûé êàïîò',1),(3,3,'êðàñíàÿ äâåðü',1),(4,4,'äâèãàòåëü V8',0),(5,5,'êîëåñî 13`',0),(6,1,'çåëåíûé áàãàæíèê',2),(7,2,'çåëåíûé êàïîò',2),(8,3,'çåëåíàÿ äâåðü',2),(9,4,'äâèãàòåëü V4',0),(10,5,'êîëåñî 15`',0),(11,1,'ñèíèé áàãàæíèê',3),(12,2,'ñèíèé êàïîò',3),(13,3,'ñèíÿÿ äâåðü',3),(14,4,'äâèãàòåëü R4',0),(15,5,'êîëåñî 17`',0),(16,1,'æåëòûé áàãàæíèê',4),(17,2,'æåëòûé êàïîò',4),(18,3,'æåëòàÿ äâåðü',4),(19,4,'äâèãàòåëü R8',0),(20,5,'êîëåñî 19`',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `part` ENABLE KEYS */;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `report_id` int(11) NOT NULL auto_increment,
  `report_name` varchar(100) character set utf8 default NULL,
  `measure_id` int(11) NOT NULL default '0',
  `subtotal_id` int(11) default '0',
  `measure2_id` int(11) NOT NULL default '0',
  `dynamic` int(11) default '0',
  PRIMARY KEY  (`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `report`
--


/*!40000 ALTER TABLE `report` DISABLE KEYS */;
LOCK TABLES `report` WRITE;
INSERT INTO `report` VALUES (3,'Ñòîèìîñòü ïîñòàâëåíûõ ïîñòàâùèêàìè äåòàëåé çà ïåðèîä âðåìåíè ñ ïîäúèòîãàìè ïî ãîðîäó',2,1,0,1),(4,'Ñóììàðíîå ÷èñëî äåòàëåé ïî òèïàì çà ïåðèîä âðåìåíè',4,0,0,1),(5,'Ãðàôèê ïîñòàâêè äåòàëè ïî íåäåëÿì',9,0,0,0),(6,'Ñòîèìîñòü  è êà÷åñòâî ïîñòàâîê èç Ìîñêâû',2,0,0,1),(7,'Êòî êàêèå òèïû äåòàëåé íà êàêóþ ñòîèìîñòü ïîñòàâèë',2,0,4,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `report` ENABLE KEYS */;

--
-- Table structure for table `report_column`
--

DROP TABLE IF EXISTS `report_column`;
CREATE TABLE `report_column` (
  `report_id` int(11) NOT NULL default '0',
  `field_id` int(11) NOT NULL default '0',
  `fn_id` int(11) default NULL,
  UNIQUE KEY `report_column_index` (`report_id`,`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `report_column`
--


/*!40000 ALTER TABLE `report_column` DISABLE KEYS */;
LOCK TABLES `report_column` WRITE;
INSERT INTO `report_column` VALUES (3,12,1),(4,13,1),(5,13,1),(6,12,1),(6,14,2),(7,12,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `report_column` ENABLE KEYS */;

--
-- Table structure for table `report_filter`
--

DROP TABLE IF EXISTS `report_filter`;
CREATE TABLE `report_filter` (
  `report_id` int(11) NOT NULL default '0',
  `field_id` int(11) NOT NULL default '0',
  `default_value` varchar(100) default '',
  `user_change` int(11) default '1',
  `signum_id` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `report_filter`
--


/*!40000 ALTER TABLE `report_filter` DISABLE KEYS */;
LOCK TABLES `report_filter` WRITE;
INSERT INTO `report_filter` VALUES (3,10,'3',1,0),(4,10,'2',1,0),(5,5,'3',1,0),(6,10,'1',1,0),(6,1,'1',0,0),(7,10,'3',1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `report_filter` ENABLE KEYS */;

--
-- Table structure for table `report_orderby`
--

DROP TABLE IF EXISTS `report_orderby`;
CREATE TABLE `report_orderby` (
  `report_id` int(11) NOT NULL default '0',
  `field_id` int(11) NOT NULL default '0',
  `dir_id` int(11) default '1'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `report_orderby`
--


/*!40000 ALTER TABLE `report_orderby` DISABLE KEYS */;
LOCK TABLES `report_orderby` WRITE;
INSERT INTO `report_orderby` VALUES (3,2,1),(4,4,1),(5,9,1),(6,2,1),(7,2,1),(7,4,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `report_orderby` ENABLE KEYS */;

--
-- Table structure for table `signum`
--

DROP TABLE IF EXISTS `signum`;
CREATE TABLE `signum` (
  `signum_id` int(11) NOT NULL auto_increment,
  `signum_name` varchar(100) default NULL,
  PRIMARY KEY  (`signum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `signum`
--


/*!40000 ALTER TABLE `signum` DISABLE KEYS */;
LOCK TABLES `signum` WRITE;
INSERT INTO `signum` VALUES (1,'>'),(2,'<');
UNLOCK TABLES;
/*!40000 ALTER TABLE `signum` ENABLE KEYS */;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `status_id` int(11) NOT NULL auto_increment,
  `status_name` varchar(100) character set utf8 default NULL,
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `status`
--


/*!40000 ALTER TABLE `status` DISABLE KEYS */;
LOCK TABLES `status` WRITE;
INSERT INTO `status` VALUES (1,'Íîâè÷îê'),(2,'Ïàðòíåð'),(3,'Ïðîâåðåííûé ïàðòíåð');
UNLOCK TABLES;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

--
-- Table structure for table `supply`
--

DROP TABLE IF EXISTS `supply`;
CREATE TABLE `supply` (
  `supply_id` int(11) NOT NULL auto_increment,
  `firm_id` int(11) NOT NULL default '0',
  `part_id` int(11) NOT NULL default '0',
  `supply_date` date default NULL,
  `quantity` int(11) default NULL,
  `price` float default NULL,
  `quality` float default NULL,
  `supply_code` varchar(10) default NULL,
  PRIMARY KEY  (`supply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `supply`
--


/*!40000 ALTER TABLE `supply` DISABLE KEYS */;
LOCK TABLES `supply` WRITE;
INSERT INTO `supply` VALUES (1,1,1,'2006-09-17',79,27,0.58,'supply1_1'),(2,2,1,'2006-08-24',16,68,0.6,'supply2_1'),(3,3,1,'2006-10-21',15,14,0.93,'supply3_1'),(4,5,1,'2006-09-28',10,14,0.59,'supply5_1'),(5,7,1,'2006-09-30',85,64,0.91,'supply7_1'),(6,1,2,'2006-09-14',74,10,0.91,'supply1_2'),(7,2,2,'2006-09-27',63,50,0.9,'supply2_2'),(8,3,2,'2006-09-26',85,30,0.93,'supply3_2'),(9,5,2,'2006-08-30',25,48,0.78,'supply5_2'),(10,7,2,'2006-09-21',17,50,0.51,'supply7_2'),(11,1,3,'2006-09-19',45,32,0.92,'supply1_3'),(12,2,3,'2006-09-12',45,43,0.54,'supply2_3'),(13,3,3,'2006-10-15',99,79,0.69,'supply3_3'),(14,5,3,'2006-09-15',43,86,0.91,'supply5_3'),(15,7,3,'2006-09-29',62,62,0.99,'supply7_3'),(16,2,4,'2006-10-14',88,53,0.97,'supply2_4'),(17,3,4,'2006-10-17',8,57,0.66,'supply3_4'),(18,6,4,'2006-08-26',55,19,0.6,'supply6_4'),(19,2,5,'2006-08-26',13,38,0.59,'supply2_5'),(20,3,5,'2006-09-03',17,60,0.68,'supply3_5'),(21,4,5,'2006-09-07',51,81,0.67,'supply4_5'),(22,1,6,'2006-09-23',50,49,0.59,'supply1_1'),(23,2,6,'2006-10-08',98,61,0.75,'supply2_1'),(24,3,6,'2006-09-10',29,63,0.7,'supply3_1'),(25,5,6,'2006-10-14',29,53,0.73,'supply5_1'),(26,7,6,'2006-10-08',30,76,0.88,'supply7_1'),(27,1,7,'2006-10-08',18,31,0.96,'supply1_2'),(28,2,7,'2006-09-03',26,64,0.51,'supply2_2'),(29,3,7,'2006-10-07',40,71,0.83,'supply3_2'),(30,5,7,'2006-10-09',94,100,0.97,'supply5_2'),(31,7,7,'2006-08-26',72,48,0.71,'supply7_2'),(32,1,8,'2006-10-12',8,61,0.62,'supply1_3'),(33,2,8,'2006-10-18',87,36,0.99,'supply2_3'),(34,3,8,'2006-09-27',43,20,0.85,'supply3_3'),(35,5,8,'2006-09-29',60,11,0.51,'supply5_3'),(36,7,8,'2006-09-09',74,55,0.52,'supply7_3'),(37,2,9,'2006-09-15',67,48,0.68,'supply2_4'),(38,3,9,'2006-09-03',76,38,0.52,'supply3_4'),(39,6,9,'2006-09-20',83,21,0.86,'supply6_4'),(40,2,10,'2006-09-23',24,46,0.73,'supply2_5'),(41,3,10,'2006-08-30',82,7,0.93,'supply3_5'),(42,4,10,'2006-09-25',93,53,0.5,'supply4_5'),(43,1,11,'2006-09-27',40,79,0.75,'supply1_1'),(44,2,11,'2006-09-23',58,33,0.73,'supply2_1'),(45,3,11,'2006-08-31',32,12,0.72,'supply3_1'),(46,5,11,'2006-10-04',95,64,0.77,'supply5_1'),(47,7,11,'2006-10-22',32,84,0.69,'supply7_1'),(48,1,12,'2006-09-11',77,18,0.52,'supply1_2'),(49,2,12,'2006-09-18',15,66,0.55,'supply2_2'),(50,3,12,'2006-09-29',81,72,0.84,'supply3_2'),(51,5,12,'2006-10-03',58,34,0.68,'supply5_2'),(52,7,12,'2006-10-15',80,100,0.61,'supply7_2'),(53,1,13,'2006-10-22',33,65,0.94,'supply1_3'),(54,2,13,'2006-09-12',51,77,0.71,'supply2_3'),(55,3,13,'2006-10-01',18,32,0.71,'supply3_3'),(56,5,13,'2006-09-06',68,79,0.5,'supply5_3'),(57,7,13,'2006-10-14',86,11,0.88,'supply7_3'),(58,2,14,'2006-10-12',6,79,0.87,'supply2_4'),(59,3,14,'2006-10-16',13,78,0.8,'supply3_4'),(60,6,14,'2006-09-14',40,54,0.69,'supply6_4'),(61,2,15,'2006-10-22',73,4,0.85,'supply2_5'),(62,3,15,'2006-08-29',45,78,0.83,'supply3_5'),(63,4,15,'2006-09-18',42,69,0.93,'supply4_5'),(64,1,16,'2006-09-09',96,26,0.67,'supply1_1'),(65,2,16,'2006-09-18',94,45,0.88,'supply2_1'),(66,3,16,'2006-09-01',69,74,0.58,'supply3_1'),(67,5,16,'2006-09-07',5,7,0.59,'supply5_1'),(68,7,16,'2006-10-21',37,95,0.96,'supply7_1'),(69,1,17,'2006-08-31',50,32,0.96,'supply1_2'),(70,2,17,'2006-10-22',77,66,0.64,'supply2_2'),(71,3,17,'2006-09-12',3,94,0.82,'supply3_2'),(72,5,17,'2006-09-19',62,61,0.53,'supply5_2'),(73,7,17,'2006-08-30',40,66,0.57,'supply7_2'),(74,1,18,'2006-09-29',69,55,1,'supply1_3'),(75,2,18,'2006-10-05',35,25,0.58,'supply2_3'),(76,3,18,'2006-10-05',72,23,0.91,'supply3_3'),(77,5,18,'2006-09-15',64,47,0.74,'supply5_3'),(78,7,18,'2006-08-24',46,41,0.64,'supply7_3'),(79,2,19,'2006-09-14',2,4,0.9,'supply2_4'),(80,3,19,'2006-10-07',66,84,0.57,'supply3_4'),(81,6,19,'2006-10-18',23,75,0.96,'supply6_4'),(82,2,20,'2006-09-03',7,24,0.58,'supply2_5'),(83,3,20,'2006-09-11',3,31,0.76,'supply3_5'),(84,4,20,'2006-09-26',44,40,0.54,'supply4_5');
UNLOCK TABLES;
/*!40000 ALTER TABLE `supply` ENABLE KEYS */;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `type_id` int(11) NOT NULL auto_increment,
  `type_name` varchar(100) character set utf8 default NULL,
  PRIMARY KEY  (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `type`
--


/*!40000 ALTER TABLE `type` DISABLE KEYS */;
LOCK TABLES `type` WRITE;
INSERT INTO `type` VALUES (1,'áàãàæíèê'),(2,'êàïîò'),(3,'äâåðü'),(4,'äâèãàòåëü'),(5,'êîëåñî');
UNLOCK TABLES;
/*!40000 ALTER TABLE `type` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

